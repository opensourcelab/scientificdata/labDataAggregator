# LabDataAggregator

SiLA / Influx DB / Grafana based time series data aggregator microservice with SiLA interface for scientific data acquisition, aggregation and visualisation.

## Description

The LabDataAggregator (LARA) is a microservice for the aggregation of time series data from arbitrary SiLA servers. The data is stored in an InfluxDB time series database and can be visualized with Grafana. The microservice is designed to be easily extendable and can be used in a wide range of scientific applications. The microservice is dockerized and can be started with a single docker-compose command.
Very importantly, all measurement data receive an **automatic annotation with semantic metadata (JSON-LD)** - which is then also written into the Parquet / SciDatS file/stream during aggregation.
This results in machine-actionable data that can be easily shared and reused in the scientific community (e.g. in the context of **FAIR data principles**).

## Features

- [x] time series data aggregator (polling and streaming modes) of arbitrary SiLA servers (e.g. temperature, pressure, pH, light intensity sensors)
- [x] InfluxDB as time series database
- [x] Grafana for beautiful and customisable data visualization (Dashboards)
- [x] SiLA communication for data aggregation and aggregation configuration
- [x] Aggregation of data to pandas Dataframe / pyarrow parquet with metadata (e.g. "aggregate measurement X from Mon, 8:30h - Sat 19:15h and write everything into a table=Dataframe, add metadata in JSON-LD to the measurement")
- [x] Automatic semantic annotation of data: All measurement data receive an automatic annotation with semantic metadata (JSON-LD) - which is then also written into the Parquet / SciDatS file/stream during aggregation
- [x] The microservice is completely dockerized and can be started with a single docker-compose command

### Installation

The installation process of the **LabDataAggregator** is a very simple two-step process:
Download the *docker-compose* file and the *.env* file, adjust your microservice configuration in the .env file and then run `docker compose pull and then up`, like described below:


To Run development version with docker compose:

```bash
    wget https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/raw/main/docker/docker-compose.dev-full.yaml
    wget https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/raw/main/docker/.env.dev.template
    wget https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/raw/main/docker/.env.influxdb
    wget https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/raw/main/docker/.env.grafana

 
 # or if you like curl:
    curl -O https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/blob/main/docker/docker-compose.dev-full.yaml
    curl -O https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/blob/main/docker/.env.dev.template
    curl -O https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/blob/main/docker/.env.influxdb
    curl -O https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/blob/main/docker/.env.grafana

```


**Important**:  rename .env.dev.template -> .env : 

```bash
 mv .env.dev .env
```

**Important**: in .env replace 'latest' in DOCKER_IMAGE_TAG=latest by current short [git commit hash (first 8 characters of hash)](registry.gitlab.com/opensourcelab/scientificdata/labdataaggregator/labdata-aggregator-dev:df729748).

```bash
 # example: 
 DOCKER_IMAGE_TAG=b6e72259
```

## Getting started

### Running the development version

```bash
docker-compose -f docker-compose.dev-full.yaml up
```
### Configuration

The configuration of the microservice is done via the .env file.
The configuration for the SiLA server datasources is done via a YAML feature map, send through the SiLA client to the LabDataAggregator (s. *examples folder* for example feature map).

See *jupyter/labdata_aggregator_demo.ipynb* jupyter notebook in the for a demonstration of the configuration and usage of the LabDataAggregator.

### influx DB

 - access influxDB at http://localhost:8086
 - login with credentials, specified in .env file
 - create an organisation (e.g. "org.lara")
 - create a bucket (e.g. "LARAbucket")
 - **important** store freshly generated *API token* or create and *API token* or specify the token in the .env file

### Grafana 

- access grafana at http://localhost:3000
- login with credentials, specified in .env file
- add influxDB as data source
    * specify the server: http://influxdb:8086
    * use basic auth
    * use the organisation and API token from the .env file (or generated in influxDB)
- create dashboard and panels for visualisation:
    * use the query builder from the influxDB database to create the correct queries
    * copy the query into the grafana panel configuration
    * **important** do not forget to switch on "update monitoring" in the panel configuration

### Start all SiLA Sensor / Measurement Servers

- start the SiLA servers (e.g. simple_thermostat) with the following command:

```bash
python -m simple_thermostat --insecure --port 50052
python -m simple_thermostat --insecure --port 50053

```

### Demo Jupyter Notebook

The *jupyter/labdata_aggregator_demo.ipynb* jupyter notebook demonstrates the configuration and usage of the LabDataAggregator.


## Dockumentation

The documentation is available at [https://opensourcelab.gitlab.io/scientificdata/labDataAggregator/](https://opensourcelab.gitlab.io/scientificdata/labDataAggregator/)

## Usage

1. Start docker

    cd influxDB
    docker-compose up  # use the -d flag to start it as deamon

1. Login into InfluxDB and create organisation, bucket and token


1. start SiLA data provider servers

    cd [sila server directory]
    python -m simple_thermostat --insecure --port 50052
    python -m simple_thermostat --insecure --port 50053
    python -m simple_thermostat --insecure --port 50054
    python -m simple_thermostat --insecure --port 50055

1. start labdataaggregator server

    cd SiLA
    python -m labdata_aggregator_sila --insecure --port 50066 --debug

1. open the aggregator_sila_client.ipynb (for controlling the aggreagotor)

1. change the token in the sila client and juypter notebook to the new token

1. check, if  correct feature map is loaded

1. run the sila clients

    python thermostat_sila_client.py

1. login into grafena 

    setup data connection: 
    type:  influxdb

    server:  http://influxdb:8086
    basic auth
    organisation: org.lara
    token: current influx token
    default bucket: LARAbucket

and generate Panels in the dasboard
   hint: you can use the query builder from the influxDB database to create the correct queries

1. when the panels are generated, do not forget to switch on "update monitoring"



## Support

Use the Issues to ask questions or report ideas or bugs.

## Roadmap

1. graphena visualisation
1. SiLA interface


## Contributing


## Authors and acknowledgment
mark doerr (mark.doerr@uni-greifswald.de)

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/opensourcelab/scientificdata/labDataAggregator/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


