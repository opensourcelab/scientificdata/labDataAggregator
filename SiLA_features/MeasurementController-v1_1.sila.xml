<?xml version="1.0" encoding="utf-8"?>
<Feature SiLA2Version="1.0" FeatureVersion="1.1" MaturityLevel="Draft" Originator="de.unigreifswald" Category="measurement" xmlns="http://www.sila-standard.org" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>MeasurementController</Identifier>
    <DisplayName>Measurement Controller</DisplayName>
    <Description>
    Allows to start and stop a measurement.
  </Description>
    <Command>
        <Identifier>StartMeasurement</Identifier>
        <DisplayName>StartMeasurement</DisplayName>
        <Description>
       Starting an experiment
    </Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Name</Identifier>
            <DisplayName>Name</DisplayName>
            <Description>Name of the Measurement, can be used as an identifier for this measurement.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>

        </Parameter>
        <Response>
            <Identifier>CommandExecutionUUID</Identifier>
            <DisplayName>Command Execution UUID</DisplayName>
            <Description>The Command Execution UUID according to the SiLA Standard.</Description>
            <DataType>
                <DataTypeIdentifier>UUID</DataTypeIdentifier>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>StopMeasurement</Identifier>
        <DisplayName>StopMeasurement</DisplayName>
        <Description>Stop the current Measurement</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>CommandExecutionUUID</Identifier>
            <DisplayName>Command Execution UUID</DisplayName>
            <Description>The Command Execution UUID according to the SiLA Standard.</Description>
            <DataType>
                <DataTypeIdentifier>UUID</DataTypeIdentifier>
            </DataType>
        </Parameter>
        <DefinedExecutionErrors>
            <Identifier>InvalidCommandExecutionUUID</Identifier>
            <Identifier>InvalidCommandState</Identifier>
            <Identifier>OperationNotSupported</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <!-- SetPollingInterval-->
     <Command>
        <Identifier>SetPollingInterval</Identifier>
        <DisplayName>Set Polling Interval</DisplayName>
        <Description>Set the polling interval for the Measurement Controller in seconds.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Interval</Identifier>
            <DisplayName>Interval</DisplayName>
            <Description>Polling interval in seconds (Real).</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Parameter>
     </Command>

    <!-- Feature Calls -->

    <Command>
        <Identifier>LoadFeatureCallConfigurationDB</Identifier>
        <DisplayName>Load Feature Call Configuration from the LARA database</DisplayName>
        <Description>Load a Feature Call Configuration that describes what SiLA servers to call with which commads from the LARA Database in YAML format as defined in the sila_feature_call_model.yaml LinkML data model.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>LARADatabaseConnector</Identifier>
            <DisplayName>LARA Database Connector</DisplayName>
            <Description>Connector to the LARA Database</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>Namespace</Identifier>
            <DisplayName>Namespace</DisplayName>
            <Description>Namespace of the Feature Call Configuration (YAML) to be loaded from the (LARA) database</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>FeatureCallConfigurationName</Identifier>
            <DisplayName>Feature Call Name</DisplayName>
            <Description>Name of the Feature Call Configuration (YAML) to be loaded from the (LARA) database</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Parameter>
        <DefinedExecutionErrors>
            <Identifier>InvalidFeatureCallConfiguration</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>SetFeatureCallConfiguration</Identifier>
        <DisplayName>Set new Feature Call Configuration</DisplayName>
        <Description>Set a new Feature Call Configuration that describes what SiLA servers to call with which commads. The configuration should be provided as a string in YAML format as defined in the sila_feature_call_model.yaml LinkML data model.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>FeatureCallConfiguration</Identifier>
            <DisplayName>New Feature Call Configuration (YAML string)</DisplayName>
            <Description>New Feature Call Configuration to be set - (YAML string).</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Parameter>
        <DefinedExecutionErrors>
            <Identifier>InvalidFeatureCallConfiguration</Identifier>
        </DefinedExecutionErrors>
    </Command>

    <!-- Properties-->
    
    <Property>
        <Identifier>CurrentMeasurements</Identifier>
        <DisplayName>Current Measurements</DisplayName>
        <Description>A List of Measurement Command Execution UUIDs that are currently running.</Description>
        <Observable>Yes</Observable>
        <DataType>
            <List>
                <DataType>
                    <DataTypeIdentifier>UUID</DataTypeIdentifier>
                </DataType>
            </List>
        </DataType>
    </Property>

    <!-- PollingInterval-->

    <Property>
        <Identifier>PollingInterval</Identifier>
        <DisplayName>Polling Interval</DisplayName>
        <Description>The polling interval for the Measurement Controller in seconds.</Description>
        <Observable>Yes</Observable>
        <DataType>
            <Basic>Real</Basic>
        </DataType>
    </Property>

    <Property>
        <Identifier>FeatureCallConfiguration</Identifier>
        <DisplayName>Feature Call Configuration (YAML)</DisplayName>
        <Description>Get current Feature Call Configuration (YAML string).</Description>
        <Observable>No</Observable>
        <DataType>
            <Basic>String</Basic>
        </DataType>
    </Property>

    <!-- Custom Data Types --> 
     <DataTypeDefinition>
        <Identifier>UUID</Identifier>
        <DisplayName>UUID</DisplayName>
        <Description>A Universally Unique Identifier (UUID) referring to observable command executions.</Description>
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Length>36</Length>
                    <Pattern>[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}</Pattern>
                </Constraints>
            </Constrained>
        </DataType>
    </DataTypeDefinition>

    <!-- Defined Execution Errors -->

    <DefinedExecutionError>
        <Identifier>InvalidCommandExecutionUUID</Identifier>
        <DisplayName>Invalid Command Execution UUID</DisplayName>
        <Description>
      The given Command Execution UUID does not specify a command that is currently being executed.
    </Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>InvalidCommandState</Identifier>
        <DisplayName>Invalid Command State</DisplayName>
        <Description>
      The specified command is not in a valid state to perform the operation (Pause or Resume).
    </Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>OperationNotSupported</Identifier>
        <DisplayName>Operation Not Supported</DisplayName>
        <Description>
      The operation (Pause or Resume) is not supported for the SiLA 2 command which the
      specified Command Execution UUID belongs to.
    </Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>InvalidFeatureCallConfiguration</Identifier>
        <DisplayName>Invalid Lab Configuration</DisplayName>
        <Description>Invalid Lab Configuration</Description>
    </DefinedExecutionError>
 
</Feature>