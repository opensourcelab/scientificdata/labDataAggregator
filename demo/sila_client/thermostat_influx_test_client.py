import time
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS

#with InfluxDBClient(url="http://localhost:8086", token=token, org=org) as client:

org = "antimoney"
bucket = "cultivation"
influx_port = '8086'
token="BQxg4ENIlWNFcGT0pJQSMWFjs1ajv6cKcx1c-rqjnqtOtuMlWNg2NZCsYUri0b9n5cr9qSS6dBG7dTI5EZMS6A=="
measurement='temperature_thermostat'
thermostat="thermostat_1"

influx_client = InfluxDBClient(url="http://localhost:8086", token=token, org=org)

write_api = influx_client.write_api(write_options=SYNCHRONOUS)


for i in range(50):
    curr_temp = i*1.2
    curr_timestamp = 1

    print(f"ts: {curr_timestamp} - temp: {curr_temp}")
    #data = f"{measurement},thermostat={thermostat} temp={curr_temp}"


    data = {  "measurement": measurement,
                        "tags": {"thermostat": thermostat },
                        "fields": {
                        "temperature": curr_temp
                     },}
    write_api.write(bucket, org, data)
    time.sleep(2)


influx_client.close()


