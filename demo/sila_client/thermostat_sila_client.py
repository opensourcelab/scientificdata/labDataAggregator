

import time
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS

from simple_thermostat import Client

#with InfluxDBClient(url="http://localhost:8086", token=token, org=org) as client:

# ---------  setting up influxDB
org = "antimoney"
bucket = "cultivation"
influx_port = '8086'
token="-NZ11XUKLpHOLV9eIZ2d202eLDwqnofmKS-7DRgsAAxjsirmihEnQHRnq7Y-0vEGLyIuI1Cx4YvwOFXvAMhsJQ=="
measurement='temperature_thermostat'
thermostat_1="sila-thermostat_1"
thermostat_2="sila-thermostat_2"
thermostat_3="sila-thermostat_3"
thermostat_4="sila-thermostat_4"

influx_client = InfluxDBClient(url="http://localhost:8086", token=token, org=org)

write_api = influx_client.write_api(write_options=SYNCHRONOUS)

# ---------- SiLA clients -----

# these operations will be automated, based on a lab automation configuration file


sila_thermostat_client_1 = Client("127.0.0.1", 50052, insecure=True)
sila_thermostat_client_1.TemperatureController.SetMaxTemperature(44.0)
curr_temp_1 = sila_thermostat_client_1.TemperatureController.CurrentTemperature.get()

sila_thermostat_client_2 = Client("127.0.0.1", 50053, insecure=True)
sila_thermostat_client_2.TemperatureController.SetMaxTemperature(44.0)
curr_temp_2 = sila_thermostat_client_2.TemperatureController.CurrentTemperature.get()


sila_thermostat_client_3 = Client("127.0.0.1", 50054, insecure=True)
sila_thermostat_client_3.TemperatureController.SetMaxTemperature(44.0)
curr_temp_3 = sila_thermostat_client_3.TemperatureController.CurrentTemperature.get()


sila_thermostat_client_4 = Client("127.0.0.1", 50055, insecure=True)
sila_thermostat_client_4.TemperatureController.SetMaxTemperature(44.0)
curr_temp_4 = sila_thermostat_client_4.TemperatureController.CurrentTemperature.get()


for i in range(50):
    curr_temp_1 = sila_thermostat_client_1.TemperatureController.CurrentTemperature.get()
    curr_timestamp_1 = sila_thermostat_client_1.TemperatureController.CurrentTimestamp.get()

    print(f"ts1: {curr_timestamp_1} - temp: {curr_temp_1}")

    #data = f"{measurement},thermostat={thermostat} temp={curr_temp_1}"
    data = { "measurement": measurement,
             "tags": {"thermostat": thermostat_1 },
                      "fields": {
                      "temperature": curr_temp_1,
                     },}
    write_api.write(bucket, org, data)

    curr_temp_2 = sila_thermostat_client_2.TemperatureController.CurrentTemperature.get()
    curr_timestamp_2 = sila_thermostat_client_2.TemperatureController.CurrentTimestamp.get()

    print(f"ts2: {curr_timestamp_2} - temp: {curr_temp_2}")

   
    data = { "measurement": measurement,
             "tags": {"thermostat": thermostat_2 },
                      "fields": {
                      "temperature": curr_temp_2,
                     },}
    write_api.write(bucket, org, data)


    curr_temp_3 = sila_thermostat_client_3.TemperatureController.CurrentTemperature.get()
    curr_timestamp_3 = sila_thermostat_client_3.TemperatureController.CurrentTimestamp.get()

    print(f"ts3: {curr_timestamp_3} - temp: {curr_temp_3}")

   
    data = { "measurement": measurement,
             "tags": {"thermostat": thermostat_3 },
                      "fields": {
                      "temperature": curr_temp_3,
                     },}
    write_api.write(bucket, org, data)

    curr_temp_4 = sila_thermostat_client_4.TemperatureController.CurrentTemperature.get()
    curr_timestamp_4 = sila_thermostat_client_4.TemperatureController.CurrentTimestamp.get()

    print(f"ts4: {curr_timestamp_4} - temp: {curr_temp_4}")

   
    data = { "measurement": measurement,
             "tags": {"thermostat": thermostat_4 },
                      "fields": {
                      "temperature": curr_temp_4,
                     },}
    write_api.write(bucket, org, data)

    time.sleep(2)


#sila_thermostat_client_1.close()

# aggregate_data()

influx_client.close()

