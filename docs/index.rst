Welcome to Lab Data Aggregator's documentation!
======================================================================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   sila_feature_call_config/*
   source/modules
   development
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
