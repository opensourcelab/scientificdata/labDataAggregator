.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Lab Data Aggregator, run this command in your terminal:

.. code-block:: console

    $ pip install labdata_aggregator

This is the preferred method to install Lab Data Aggregator, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From source
-----------

