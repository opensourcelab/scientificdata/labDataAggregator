

# Class: CallParameters


_"The parameters of the SiLA call."_





URI: [https://w3id.org/SiLA/FeatureCall/:CallParameters](https://w3id.org/SiLA/FeatureCall/:CallParameters)






```mermaid
 classDiagram
    class CallParameters
    click CallParameters href "../CallParameters"
      CallParameters : name
        
      CallParameters : value
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [name](name.md) | 1 <br/> [String](String.md) | "The name of the parameter | direct |
| [value](value.md) | 1 <br/> [String](String.md) | "The value of the parameter | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | [parameters](parameters.md) | range | [CallParameters](CallParameters.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://w3id.org/SiLA/FeatureCall/:CallParameters |
| native | https://w3id.org/SiLA/FeatureCall/:CallParameters |







## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: CallParameters
description: '"The parameters of the SiLA call."'
from_schema: https://w3id.org/SiLA/FeatureCall
attributes:
  name:
    name: name
    description: '"The name of the parameter."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementParameterName
    domain_of:
    - CallParameters
    range: string
    required: true
  value:
    name: value
    description: '"The value of the parameter."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementParameterValue
    domain_of:
    - CallParameters
    range: string
    required: true

```
</details>

### Induced

<details>
```yaml
name: CallParameters
description: '"The parameters of the SiLA call."'
from_schema: https://w3id.org/SiLA/FeatureCall
attributes:
  name:
    name: name
    description: '"The name of the parameter."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementParameterName
    alias: name
    owner: CallParameters
    domain_of:
    - CallParameters
    range: string
    required: true
  value:
    name: value
    description: '"The value of the parameter."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementParameterValue
    alias: value
    owner: CallParameters
    domain_of:
    - CallParameters
    range: string
    required: true

```
</details>