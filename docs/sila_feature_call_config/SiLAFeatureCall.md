

# Class: SiLAFeatureCall


_"The SiLA Feature Call represents one single SiLA feature call to trigger a measurement."_





URI: [https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCall](https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCall)






```mermaid
 classDiagram
    class SiLAFeatureCall
    click SiLAFeatureCall href "../SiLAFeatureCall"
      SiLAFeatureCall : certificate
        
      SiLAFeatureCall : feature
        
      SiLAFeatureCall : ip_address
        
      SiLAFeatureCall : parameters
        
          
    
    
    SiLAFeatureCall --> "*" CallParameters : parameters
    click CallParameters href "../CallParameters"

        
      SiLAFeatureCall : poll_interval
        
      SiLAFeatureCall : port
        
      SiLAFeatureCall : private_key
        
      SiLAFeatureCall : quantity
        
      SiLAFeatureCall : sila_call_metadata
        
      SiLAFeatureCall : sila_call_observable
        
      SiLAFeatureCall : sila_call_value_calibrated
        
      SiLAFeatureCall : sila_call_value_raw
        
      SiLAFeatureCall : sila_call_value_unit
        
      SiLAFeatureCall : unit
        
      
```




<!-- no inheritance hierarchy -->


## Slots

| Name | Cardinality and Range | Description | Inheritance |
| ---  | --- | --- | --- |
| [feature](feature.md) | 1 <br/> [String](String.md) | "Fully qualified SiLA feature | direct |
| [quantity](quantity.md) | 1 <br/> [String](String.md) | "The quantity of the measurement | direct |
| [unit](unit.md) | 1 <br/> [String](String.md) | "The unit of the measurement | direct |
| [sila_call_value_raw](sila_call_value_raw.md) | 1 <br/> [String](String.md) | "The SiLA call to get a single raw value the measurement | direct |
| [sila_call_value_calibrated](sila_call_value_calibrated.md) | 0..1 <br/> [String](String.md) | "The SiLA call to get a single calibrated value the measurement | direct |
| [sila_call_value_unit](sila_call_value_unit.md) | 0..1 <br/> [String](String.md) | "The SiLA call to get the unit of the value the measurement | direct |
| [sila_call_observable](sila_call_observable.md) | 1 <br/> [Boolean](Boolean.md) | "Is the SiLA call observable ?" | direct |
| [sila_call_metadata](sila_call_metadata.md) | 1 <br/> [String](String.md) | "The SiLA call to retrieve the metadata of the measurement | direct |
| [parameters](parameters.md) | * <br/> [CallParameters](CallParameters.md) | "The parameters of the measurement | direct |
| [poll_interval](poll_interval.md) | 1 <br/> [Integer](Integer.md) | "The interval in seconds to poll the measurement | direct |
| [certificate](certificate.md) | 0..1 <br/> [String](String.md) | "The name of the certificate to authenticate the SiLA call | direct |
| [private_key](private_key.md) | 0..1 <br/> [String](String.md) | "The name of the private key to authenticate the SiLA call | direct |
| [ip_address](ip_address.md) | 0..1 <br/> [String](String.md) | "The IP address of the SiLA server | direct |
| [port](port.md) | 0..1 <br/> [Integer](Integer.md) | "The port of the SiLA server | direct |





## Usages

| used by | used in | type | used |
| ---  | --- | --- | --- |
| [SiLAFeatureCalls](SiLAFeatureCalls.md) | [SiLAFeatureCalls](SiLAFeatureCalls.md) | range | [SiLAFeatureCall](SiLAFeatureCall.md) |






## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCall |
| native | https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCall |







## LinkML Source

<!-- TODO: investigate https://stackoverflow.com/questions/37606292/how-to-create-tabbed-code-blocks-in-mkdocs-or-sphinx -->

### Direct

<details>
```yaml
name: SiLAFeatureCall
description: '"The SiLA Feature Call represents one single SiLA feature call to trigger
  a measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
slots:
- feature
- quantity
- unit
- sila_call_value_raw
- sila_call_value_calibrated
- sila_call_value_unit
- sila_call_observable
- sila_call_metadata
- parameters
- poll_interval
- certificate
- private_key
- ip_address
- port

```
</details>

### Induced

<details>
```yaml
name: SiLAFeatureCall
description: '"The SiLA Feature Call represents one single SiLA feature call to trigger
  a measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
attributes:
  feature:
    name: feature
    description: '"Fully qualified SiLA feature."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:sila_feature
    identifier: true
    alias: feature
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: true
  quantity:
    name: quantity
    description: '"The quantity of the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementQuantity
    alias: quantity
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: true
  unit:
    name: unit
    description: '"The unit of the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementUnit
    alias: unit
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: true
  sila_call_value_raw:
    name: sila_call_value_raw
    description: '"The SiLA call to get a single raw value the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementCallValueRaw
    alias: sila_call_value_raw
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: true
  sila_call_value_calibrated:
    name: sila_call_value_calibrated
    description: '"The SiLA call to get a single calibrated value the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementCallValueCalibrated
    alias: sila_call_value_calibrated
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: false
  sila_call_value_unit:
    name: sila_call_value_unit
    description: '"The SiLA call to get the unit of the value the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementCallValueUnit
    alias: sila_call_value_unit
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: false
  sila_call_observable:
    name: sila_call_observable
    description: '"Is the SiLA call observable ?"'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    alias: sila_call_observable
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: boolean
    required: true
  sila_call_metadata:
    name: sila_call_metadata
    description: '"The SiLA call to retrieve the metadata of the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementMetadataCall
    alias: sila_call_metadata
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: true
  parameters:
    name: parameters
    description: '"The parameters of the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementParameters
    alias: parameters
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: CallParameters
    multivalued: true
  poll_interval:
    name: poll_interval
    description: '"The interval in seconds to poll the measurement."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementPollInterval
    alias: poll_interval
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: integer
    required: true
  certificate:
    name: certificate
    description: '"The name of the certificate to authenticate the SiLA call."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementSiLACertificate
    alias: certificate
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: false
  private_key:
    name: private_key
    description: '"The name of the private key to authenticate the SiLA call."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/measurementSiLAPrivateKey
    alias: private_key
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: false
  ip_address:
    name: ip_address
    description: '"The IP address of the SiLA server."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/IPAddress
    alias: ip_address
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: string
    required: false
  port:
    name: port
    description: '"The port of the SiLA server."'
    from_schema: https://w3id.org/SiLA/FeatureCall
    rank: 1000
    slot_uri: oso:measurement/TCPPort
    alias: port
    owner: SiLAFeatureCall
    domain_of:
    - SiLAFeatureCall
    range: integer
    required: false

```
</details>