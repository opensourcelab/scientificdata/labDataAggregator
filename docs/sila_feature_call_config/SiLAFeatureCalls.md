

# Slot: SiLAFeatureCalls

URI: [https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCalls](https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCalls)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCalls](SiLAFeatureCalls.md) |  |  no  |







## Properties

* Range: [SiLAFeatureCall](SiLAFeatureCall.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCalls |
| native | https://w3id.org/SiLA/FeatureCall/:SiLAFeatureCalls |




## LinkML Source

<details>
```yaml
name: SiLAFeatureCalls
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
alias: SiLAFeatureCalls
owner: SiLAFeatureCalls
domain_of:
- SiLAFeatureCalls
range: SiLAFeatureCall
multivalued: true
inlined: true
inlined_as_list: false

```
</details>