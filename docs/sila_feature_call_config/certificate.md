

# Slot: certificate


_"The name of the certificate to authenticate the SiLA call."_



URI: [oso:measurement/measurementSiLACertificate](http://w3c.org/oso/measurement/measurementSiLACertificate)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementSiLACertificate |
| native | https://w3id.org/SiLA/FeatureCall/:certificate |




## LinkML Source

<details>
```yaml
name: certificate
description: '"The name of the certificate to authenticate the SiLA call."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementSiLACertificate
alias: certificate
domain_of:
- SiLAFeatureCall
range: string
required: false

```
</details>