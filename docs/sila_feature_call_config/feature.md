

# Slot: feature


_"Fully qualified SiLA feature."_



URI: [oso:sila_feature](http://w3c.org/oso/sila_feature)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:sila_feature |
| native | https://w3id.org/SiLA/FeatureCall/:feature |




## LinkML Source

<details>
```yaml
name: feature
description: '"Fully qualified SiLA feature."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:sila_feature
identifier: true
alias: feature
domain_of:
- SiLAFeatureCall
range: string
required: true

```
</details>