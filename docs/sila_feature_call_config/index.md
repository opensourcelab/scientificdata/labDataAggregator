# SiLAFeatureCall

SiLA feature call.

URI: https://w3id.org/SiLA/FeatureCall

Name: SiLAFeatureCall



## Classes

| Class | Description |
| --- | --- |
| [CallParameters](CallParameters.md) | "The parameters of the SiLA call." |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a measurement." |
| [SiLAFeatureCalls](SiLAFeatureCalls.md) | None |



## Slots

| Slot | Description |
| --- | --- |
| [certificate](certificate.md) | "The name of the certificate to authenticate the SiLA call |
| [feature](feature.md) | "Fully qualified SiLA feature |
| [ip_address](ip_address.md) | "The IP address of the SiLA server |
| [name](name.md) | "The name of the parameter |
| [parameters](parameters.md) | "The parameters of the measurement |
| [poll_interval](poll_interval.md) | "The interval in seconds to poll the measurement |
| [port](port.md) | "The port of the SiLA server |
| [private_key](private_key.md) | "The name of the private key to authenticate the SiLA call |
| [quantity](quantity.md) | "The quantity of the measurement |
| [sila_call_metadata](sila_call_metadata.md) | "The SiLA call to retrieve the metadata of the measurement |
| [sila_call_observable](sila_call_observable.md) | "Is the SiLA call observable ?" |
| [sila_call_value_calibrated](sila_call_value_calibrated.md) | "The SiLA call to get a single calibrated value the measurement |
| [sila_call_value_raw](sila_call_value_raw.md) | "The SiLA call to get a single raw value the measurement |
| [sila_call_value_unit](sila_call_value_unit.md) | "The SiLA call to get the unit of the value the measurement |
| [SiLAFeatureCalls](SiLAFeatureCalls.md) |  |
| [unit](unit.md) | "The unit of the measurement |
| [value](value.md) | "The value of the parameter |


## Enumerations

| Enumeration | Description |
| --- | --- |


## Types

| Type | Description |
| --- | --- |
| [Boolean](Boolean.md) | A binary (true or false) value |
| [Curie](Curie.md) | a compact URI |
| [Date](Date.md) | a date (year, month and day) in an idealized calendar |
| [DateOrDatetime](DateOrDatetime.md) | Either a date or a datetime |
| [Datetime](Datetime.md) | The combination of a date and time |
| [Decimal](Decimal.md) | A real number with arbitrary precision that conforms to the xsd:decimal speci... |
| [Double](Double.md) | A real number that conforms to the xsd:double specification |
| [Float](Float.md) | A real number that conforms to the xsd:float specification |
| [Integer](Integer.md) | An integer |
| [Jsonpath](Jsonpath.md) | A string encoding a JSON Path |
| [Jsonpointer](Jsonpointer.md) | A string encoding a JSON Pointer |
| [Ncname](Ncname.md) | Prefix part of CURIE |
| [Nodeidentifier](Nodeidentifier.md) | A URI, CURIE or BNODE that represents a node in a model |
| [Objectidentifier](Objectidentifier.md) | A URI or CURIE that represents an object in the model |
| [Sparqlpath](Sparqlpath.md) | A string encoding a SPARQL Property Path |
| [String](String.md) | A character string |
| [Time](Time.md) | A time object represents a (local) time of day, independent of any particular... |
| [Uri](Uri.md) | a complete URI |
| [Uriorcurie](Uriorcurie.md) | a URI or a CURIE |


## Subsets

| Subset | Description |
| --- | --- |
