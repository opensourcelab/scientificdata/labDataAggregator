

# Slot: ip_address


_"The IP address of the SiLA server."_



URI: [oso:measurement/IPAddress](http://w3c.org/oso/measurement/IPAddress)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/IPAddress |
| native | https://w3id.org/SiLA/FeatureCall/:ip_address |




## LinkML Source

<details>
```yaml
name: ip_address
description: '"The IP address of the SiLA server."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/IPAddress
alias: ip_address
domain_of:
- SiLAFeatureCall
range: string
required: false

```
</details>