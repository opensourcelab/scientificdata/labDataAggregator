

# Slot: name


_"The name of the parameter."_



URI: [oso:measurement/measurementParameterName](http://w3c.org/oso/measurement/measurementParameterName)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [CallParameters](CallParameters.md) | "The parameters of the SiLA call |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementParameterName |
| native | https://w3id.org/SiLA/FeatureCall/:name |




## LinkML Source

<details>
```yaml
name: name
description: '"The name of the parameter."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementParameterName
alias: name
owner: CallParameters
domain_of:
- CallParameters
range: string
required: true

```
</details>