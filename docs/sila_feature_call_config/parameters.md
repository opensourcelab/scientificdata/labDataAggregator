

# Slot: parameters


_"The parameters of the measurement."_



URI: [oso:measurement/measurementParameters](http://w3c.org/oso/measurement/measurementParameters)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [CallParameters](CallParameters.md)

* Multivalued: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementParameters |
| native | https://w3id.org/SiLA/FeatureCall/:parameters |




## LinkML Source

<details>
```yaml
name: parameters
description: '"The parameters of the measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementParameters
alias: parameters
domain_of:
- SiLAFeatureCall
range: CallParameters
multivalued: true

```
</details>