

# Slot: poll_interval


_"The interval in seconds to poll the measurement."_



URI: [oso:measurement/measurementPollInterval](http://w3c.org/oso/measurement/measurementPollInterval)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [Integer](Integer.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementPollInterval |
| native | https://w3id.org/SiLA/FeatureCall/:poll_interval |




## LinkML Source

<details>
```yaml
name: poll_interval
description: '"The interval in seconds to poll the measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementPollInterval
alias: poll_interval
domain_of:
- SiLAFeatureCall
range: integer
required: true

```
</details>