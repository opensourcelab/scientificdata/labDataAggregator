

# Slot: port


_"The port of the SiLA server."_



URI: [oso:measurement/TCPPort](http://w3c.org/oso/measurement/TCPPort)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [Integer](Integer.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/TCPPort |
| native | https://w3id.org/SiLA/FeatureCall/:port |




## LinkML Source

<details>
```yaml
name: port
description: '"The port of the SiLA server."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/TCPPort
alias: port
domain_of:
- SiLAFeatureCall
range: integer
required: false

```
</details>