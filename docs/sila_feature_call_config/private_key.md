

# Slot: private_key


_"The name of the private key to authenticate the SiLA call."_



URI: [oso:measurement/measurementSiLAPrivateKey](http://w3c.org/oso/measurement/measurementSiLAPrivateKey)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementSiLAPrivateKey |
| native | https://w3id.org/SiLA/FeatureCall/:private_key |




## LinkML Source

<details>
```yaml
name: private_key
description: '"The name of the private key to authenticate the SiLA call."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementSiLAPrivateKey
alias: private_key
domain_of:
- SiLAFeatureCall
range: string
required: false

```
</details>