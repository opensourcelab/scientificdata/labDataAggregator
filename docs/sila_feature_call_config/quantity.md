

# Slot: quantity


_"The quantity of the measurement."_



URI: [oso:measurement/measurementQuantity](http://w3c.org/oso/measurement/measurementQuantity)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementQuantity |
| native | https://w3id.org/SiLA/FeatureCall/:quantity |




## LinkML Source

<details>
```yaml
name: quantity
description: '"The quantity of the measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementQuantity
alias: quantity
domain_of:
- SiLAFeatureCall
range: string
required: true

```
</details>