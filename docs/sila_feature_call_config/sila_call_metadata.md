

# Slot: sila_call_metadata


_"The SiLA call to retrieve the metadata of the measurement."_



URI: [oso:measurement/measurementMetadataCall](http://w3c.org/oso/measurement/measurementMetadataCall)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementMetadataCall |
| native | https://w3id.org/SiLA/FeatureCall/:sila_call_metadata |




## LinkML Source

<details>
```yaml
name: sila_call_metadata
description: '"The SiLA call to retrieve the metadata of the measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementMetadataCall
alias: sila_call_metadata
domain_of:
- SiLAFeatureCall
range: string
required: true

```
</details>