

# Slot: sila_call_observable


_"Is the SiLA call observable ?"_



URI: [https://w3id.org/SiLA/FeatureCall/:sila_call_observable](https://w3id.org/SiLA/FeatureCall/:sila_call_observable)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [Boolean](Boolean.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | https://w3id.org/SiLA/FeatureCall/:sila_call_observable |
| native | https://w3id.org/SiLA/FeatureCall/:sila_call_observable |




## LinkML Source

<details>
```yaml
name: sila_call_observable
description: '"Is the SiLA call observable ?"'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
alias: sila_call_observable
domain_of:
- SiLAFeatureCall
range: boolean
required: true

```
</details>