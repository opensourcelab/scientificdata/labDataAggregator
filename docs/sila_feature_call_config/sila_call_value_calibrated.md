

# Slot: sila_call_value_calibrated


_"The SiLA call to get a single calibrated value the measurement."_



URI: [oso:measurement/measurementCallValueCalibrated](http://w3c.org/oso/measurement/measurementCallValueCalibrated)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementCallValueCalibrated |
| native | https://w3id.org/SiLA/FeatureCall/:sila_call_value_calibrated |




## LinkML Source

<details>
```yaml
name: sila_call_value_calibrated
description: '"The SiLA call to get a single calibrated value the measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementCallValueCalibrated
alias: sila_call_value_calibrated
domain_of:
- SiLAFeatureCall
range: string
required: false

```
</details>