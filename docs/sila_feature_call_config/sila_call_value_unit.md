

# Slot: sila_call_value_unit


_"The SiLA call to get the unit of the value the measurement."_



URI: [oso:measurement/measurementCallValueUnit](http://w3c.org/oso/measurement/measurementCallValueUnit)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementCallValueUnit |
| native | https://w3id.org/SiLA/FeatureCall/:sila_call_value_unit |




## LinkML Source

<details>
```yaml
name: sila_call_value_unit
description: '"The SiLA call to get the unit of the value the measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementCallValueUnit
alias: sila_call_value_unit
domain_of:
- SiLAFeatureCall
range: string
required: false

```
</details>