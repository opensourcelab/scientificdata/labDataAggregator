

# Slot: unit


_"The unit of the measurement."_



URI: [oso:measurement/measurementUnit](http://w3c.org/oso/measurement/measurementUnit)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [SiLAFeatureCall](SiLAFeatureCall.md) | "The SiLA Feature Call represents one single SiLA feature call to trigger a m... |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementUnit |
| native | https://w3id.org/SiLA/FeatureCall/:unit |




## LinkML Source

<details>
```yaml
name: unit
description: '"The unit of the measurement."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementUnit
alias: unit
domain_of:
- SiLAFeatureCall
range: string
required: true

```
</details>