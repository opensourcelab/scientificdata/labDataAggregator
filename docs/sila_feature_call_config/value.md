

# Slot: value


_"The value of the parameter."_



URI: [oso:measurement/measurementParameterValue](http://w3c.org/oso/measurement/measurementParameterValue)



<!-- no inheritance hierarchy -->





## Applicable Classes

| Name | Description | Modifies Slot |
| --- | --- | --- |
| [CallParameters](CallParameters.md) | "The parameters of the SiLA call |  no  |







## Properties

* Range: [String](String.md)

* Required: True





## Identifier and Mapping Information







### Schema Source


* from schema: https://w3id.org/SiLA/FeatureCall




## Mappings

| Mapping Type | Mapped Value |
| ---  | ---  |
| self | oso:measurement/measurementParameterValue |
| native | https://w3id.org/SiLA/FeatureCall/:value |




## LinkML Source

<details>
```yaml
name: value
description: '"The value of the parameter."'
from_schema: https://w3id.org/SiLA/FeatureCall
rank: 1000
slot_uri: oso:measurement/measurementParameterValue
alias: value
owner: CallParameters
domain_of:
- CallParameters
range: string
required: true

```
</details>