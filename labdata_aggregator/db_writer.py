import functools
from dataclasses import dataclass, field
from mimetypes import init
import time
from datetime import datetime
import threading
from logging import getLogger
from sila2.client import SilaClient
from sila2.discovery.browser import SilaDiscoveryBrowser

from typing import Any


logger = getLogger(__name__)


def rgetattr(obj, attr, *args):
    """_recursive version of getattr
    s. https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-subobjects-chained-properties

    :param obj: _python object_
    :type obj: _type_
    :param attr: _(dotted) attribute(s) of an object_
    :type attr: _type_
    """

    def _getattr(obj, attr):
        return getattr(obj, attr, *args)

    return functools.reduce(_getattr, [obj] + attr.split("."))


def detect_sila_servers():
    # detecting all sila clients in current network
    # TODO: use async !
    sila_disc_browser = AggregatorSilaDiscoveryBrowser(insecure=True)

    return sila_disc_browser.find_clients()


class AggregatorSilaDiscoveryBrowser(SilaDiscoveryBrowser):
    def __init__(self, insecure) -> None:
        super().__init__(insecure=insecure)

    def find_clients(self, timeout=3):
        # giving Zeroconfig some time to find servers ....
        time.sleep(timeout)
        return self.listener.services


class DataBaseWriter:
    def __init__(self) -> None:

        self._writing_interval = 1

        self.measurement_name = "de.unigreifswald/lara/lab_environment"

        self.sila_call_list = []
        self.detected_sila_clients = []

        self.influxDB_config = None
        self.measurement_thread = None
        self.stop_measure = False

        self._feature_calls = None

    # TODO: implement default feature calls

    #         sila_feature_call = dict(
    #     feature = "temperature",
    #     quantity = "temperature",
    #     unit = "K",
    #     sila_call = "temperature.get",
    #     sila_call_observable = False,
    #     sila_call_metadata = "temperature_meta.get",
    #     parameters = [{"name": "temperature", "value": "300"}],

    # )

    # sfcs = SiLAFeatureCalls(SiLAFeatureCalls=[sila_feature_call])
    # self.map_features()

    @property
    def writing_interval(self):
        return self._writing_interval

    @writing_interval.setter
    def writing_interval(self, duration=1):
        self._writing_interval = duration

    @property
    def feature_calls(self):
        return self._feature_calls

    @feature_calls.setter
    def feature_calls(self, feature_calls):
        """set the feature calls to be mapped to the SiLA clients.
        Mind that this is still the pydantic object - to access the dictionary, use
        the SiLAFeatureCalls attribute
        """
        self._feature_calls = feature_calls
        self.map_features()

    def map_features(self):
        """Map the feature calls to the currently running SiLA clients and their
        implemented features by just associating the feature call with the SiLA client.
        """

        self.sila_call_list = []

        self.detected_sila_clients = detect_sila_servers()

        if len(self.detected_sila_clients) == 0:
            logger.warning("No running Sila Servers detected !")
            return
        else:
            logger.debug(":::: detected sila clients:", self.detected_sila_clients)

            for sila_cl in self.detected_sila_clients.values():
                curr_impl_features = sila_cl.SiLAService.ImplementedFeatures.get()

                for feature in curr_impl_features:
                    try:
                        self.sila_call_list.append(
                            (sila_cl, self.feature_calls.SiLAFeatureCalls[feature])
                        )
                    except KeyError as err:
                        logger.error(f"KeyError: {err}")

    # worker
    # TODO: use threads or async for each writer ?
    def start_measurement(self, influxDB_config=None, measurement=None):
        if measurement is not None:
            self.measurement_name = measurement

        if influxDB_config is None:
            return
        else:
            self.influxDB_config = influxDB_config

        if self.measurement_thread is None:
            self.start_meas_event = threading.Event()
            self.start_meas_event.set()
            self.measurement_thread = threading.Thread(
                target=self.write_data2influxDB,
                args=(
                    self.measurement_name,
                    self._writing_interval,
                    self.start_meas_event,
                ),
                daemon=True,
            )
            logger.debug("Main    : before running thread")
            self.measurement_thread.start()
            logger.debug("Main    : wait for the thread to finish")
        else:
            self.start_meas_event.set()

        # will be replaced by threads for each writer
        # i = 0
        # while i < 50 or self.stop_measure:
        #     self.write_data2influxDB(measurement)
        #     time.sleep(1)
        #     i += 1

    def stop_measurement(self):
        # TODO: implement with threads
        print("+++ stopping experiment")
        self.start_meas_event.clear()

    def write_data2influxDB(self, measurement, writing_interval, start_event):
        waiting_interval = 4

        print("Please press ctrl-c to stop aggregation server ...")
        try:
            while True:
                logger.debug("wait_for_event_timeout starting")
                event_is_set = start_event.wait()
                logger.debug("event set: %s", event_is_set)

                if event_is_set:
                    logger.info("start event issued")
                    for sila_client, feat_call in self.sila_call_list:
                        try:
                            sila_call_value_raw = rgetattr(sila_client, feat_call.sila_call_value_raw)
                        except Exception as err:
                            logger.error(
                                f"ERROR: {err} - could not feature call for raw value: {feat_call.sila_call_value_raw}"
                            )
                        try:
                            sila_call_value_calibrated = rgetattr(
                                sila_client, feat_call.sila_call_value_calibrated
                            )
                        except Exception as err:
                            logger.error(
                                f"ERROR: {err} - could not get feature call for calibrated value: {feat_call.sila_call_value_calibrated}"
                            )
                        try:
                            sila_call_value_unit = rgetattr(
                                sila_client, feat_call.sila_call_value_unit
                            )
                        except Exception as err:
                            logger.error(
                                f"ERROR: {err} - could not get feature call for unit of value: {feat_call.sila_call_value_unit}"
                            )
                        try:
                            sila_call_metadata = rgetattr(
                                sila_client, feat_call.sila_call_metadata
                            )
                        except Exception as err:
                            logger.error(
                                f"ERROR: {err} - could not get feature call for metadata: {feat_call.sila_call_metadata}"
                            )

                        # now calling the sila servers
                        if feat_call.parameters is not None:
                            # generating a dictionary from the parameters
                            param_dict = {}
                            for key, value in feat_call.parameters:
                                param_dict[key] = value

                            try:
                                value = sila_call_value_raw(**param_dict)
                            except Exception as err:
                                logger.error(
                                    f"ERROR: {err} - could not call {feat_call.sila_call_value_raw} with parameters {param_dict}"
                                )
                                value = None
                            
                            # now with calibration
                            try:
                                value_calib = sila_call_value_calibrated(**param_dict)
                            except Exception as err:
                                logger.error(
                                    f"ERROR: {err} - could not call {feat_call.sila_call_value_calibrated} with parameters {param_dict}"
                                )
                                value_calib = None
                        else:
                            try:
                                value = sila_call_value_raw()
                            except Exception as err:
                                logger.error(
                                    f"ERROR: {err} - could not call {feat_call.sila_call_value_raw}"
                                )
                                value = None
                            try:
                                value_calib = sila_call_value_calibrated()
                            except Exception as err:
                                logger.error(
                                    f"ERROR: {err} - could not call {feat_call.sila_call_value_calibrated}"
                                )
                                value_calib = None
                        try:
                            if sila_call_value_unit is not None:
                                call_unit = sila_call_value_unit()
                        except Exception as err:
                            logger.error(
                                f"ERROR: {err} - could not call sila unit call {feat_call.sila_call_value_unit}"
                            )
                            if feat_call.unit is not None:
                                call_unit = feat_call.unit
                            else:
                                call_unit = None

                        try:
                            metadata = (sila_call_metadata())
                        except Exception as err:
                            logger.error(
                                f"ERROR: no client metadata call available {err}"
                            )
                            metadata = '{"context": ""}'  # default metadata

                        data = {
                            "measurement": measurement,
                            "tags": {
                                "device": sila_client.SiLAService.ServerName.get(),
                                "metadata": metadata,
                            },
                            "fields": {
                                feat_call.quantity: value,  # raw value
                                feat_call.quantity + "_calib": value_calib,  # add value after calibration
                                "unit": call_unit,
                            },
                        }
                        logger.debug(
                            f"data sent to influxDB {self.influxDB_config.bucket}: {data}\n"
                        )
                        print(f"data sent to influxDB: {data}\n")
                        self.influxDB_config.api.write(
                            self.influxDB_config.bucket, self.influxDB_config.org, data
                        )

                    time.sleep(writing_interval)
                else:
                    print("waiting for measurement to start ....")

                # start_event.wait(waiting_interval)
                time.sleep(waiting_interval)

        except KeyboardInterrupt:
            self.measurement_thread.stop()
