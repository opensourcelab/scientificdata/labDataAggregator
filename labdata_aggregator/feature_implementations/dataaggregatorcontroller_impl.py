# Generated by sila2.code_generator; sila2.__version__: 0.12.2
from __future__ import annotations

import json
from datetime import datetime, timedelta
from typing import TYPE_CHECKING, List

from sila2.server import MetadataDict, ObservableCommandInstance
from scidats.scidats_impl import SciDatS

from ..generated.dataaggregatorcontroller import (
    AggregateData_Responses,
    DataAggregatorControllerBase,
    SetAutoAggregation_Responses,
    SetStandardAggregationDataFormat_Responses,
)

if TYPE_CHECKING:
    from ..server import Server


class DataAggregatorControllerImpl(DataAggregatorControllerBase):
    def __init__(self, parent_server: Server) -> None:
        super().__init__(parent_server=parent_server)

        self.parent_server = parent_server

        # Default lifetime of observable command instances. Possible values:
        # None: Command instance is valid and stored in memory until server shutdown
        # datetime.timedelta: Command instance is deleted after this duration, can be increased during command runtime
        self.AggregateData_default_lifetime_of_execution = timedelta(minutes=30)

    def get_DataOutputFormats(self, *, metadata: MetadataDict) -> List[str]:
        raise NotImplementedError  # TODO

    def get_AutoMeasurementTypes(self, *, metadata: MetadataDict) -> List[str]:
        raise NotImplementedError  # TODO

    def SetStandardAggregationDataFormat(
        self, DataFormat: str, *, metadata: MetadataDict
    ) -> SetStandardAggregationDataFormat_Responses:
        raise NotImplementedError  # TODO

    def SetAutoAggregation(self, AutoAggregate: bool, *, metadata: MetadataDict) -> SetAutoAggregation_Responses:
        raise NotImplementedError  # TODO

    def AggregateData(
        self,
        Bucket: str,
        Measurement: str,
        Factor: str,
        Device: str,
        StartTime: str = "",
        EndTime: str = "",
        DataFormat: str = "JSON",
        AggregationInterval: float = 0.0, # TODO: convert to int
        *,
        metadata: MetadataDict,
        instance: ObservableCommandInstance,
    ) -> AggregateData_Responses:
        instance.begin_execution()  # set execution status from `waiting` to `running`
        influxDB_config = self.parent_server.influxdbcontroller.get_config()

        # :TODO: generalise tags  / queries


        if AggregationInterval > 0.0:
            _start_time = f"-{int(AggregationInterval)}s" if StartTime == "" else StartTime
        
        _end_time = f", end: {EndTime}" if EndTime != "" else ""  #", end: now()"


        query= f'''
from(bucket: "{Bucket}")
|> range(start: {_start_time} {_end_time})
|> filter(fn: (r) => r._measurement == "{Measurement}")
|> filter(fn: (r) => r._field == "{Factor}")
|> filter(fn: (r) => r.device == "{Device}" )'''
        

        print("influx query:", query)

        query_results_df = influxDB_config.client.query_api(
        ).query_data_frame(org=influxDB_config.org, query=query)

        metadata_core_str = query_results_df['metadata'][0]

        metadata_core_str = metadata_core_str.replace('\\n', '')

        metadata_core_dict = json.loads(metadata_core_str)

        if "JSON" == DataFormat.upper():
            # print("pq", parquet_filename)
            print("-------->\n",query_results_df.to_json())

            return AggregateData_Responses(query_results_df.to_json())
        
        elif "SCIDATS" == DataFormat.upper():
            print("... sending SciDatS (base64 encoded) ...")
            sd = SciDatS(metadata_core=metadata_core_dict, dataframe=query_results_df)

            # parquet_filename = "_".join((datetime.now().isoformat(), Measurement.replace("/", "_"), Device, Factor )) + ".parquet" 
            #sd.write_parquet(parquet_filename=parquet_filename) # writing to local parquet file for testing

            return sd.write_parquet(base64encoding=True)
