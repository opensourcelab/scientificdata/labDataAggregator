from __future__ import annotations 
from datetime import (
    datetime,
    date
)
from decimal import Decimal 
from enum import Enum 
import re
import sys
from typing import (
    Any,
    ClassVar,
    List,
    Literal,
    Dict,
    Optional,
    Union
)
from pydantic.version import VERSION  as PYDANTIC_VERSION 
if int(PYDANTIC_VERSION[0])>=2:
    from pydantic import (
        BaseModel,
        ConfigDict,
        Field,
        RootModel,
        field_validator
    )
else:
    from pydantic import (
        BaseModel,
        Field,
        validator
    )

metamodel_version = "None"
version = "0.0.1"


class ConfiguredBaseModel(BaseModel):
    model_config = ConfigDict(
        validate_assignment = True,
        validate_default = True,
        extra = "forbid",
        arbitrary_types_allowed = True,
        use_enum_values = True,
        strict = False,
    )
    pass




class LinkMLMeta(RootModel):
    root: Dict[str, Any] = {}
    model_config = ConfigDict(frozen=True)

    def __getattr__(self, key:str):
        return getattr(self.root, key)

    def __getitem__(self, key:str):
        return self.root[key]

    def __setitem__(self, key:str, value):
        self.root[key] = value

    def __contains__(self, key:str) -> bool:
        return key in self.root


linkml_meta = LinkMLMeta({'default_prefix': 'https://w3id.org/SiLA/FeatureCall/',
     'default_range': 'string',
     'description': 'SiLA feature call.',
     'id': 'https://w3id.org/SiLA/FeatureCall',
     'imports': ['linkml:types'],
     'license': 'https://creativecommons.org/publicdomain/zero/1.0/',
     'name': 'SiLAFeatureCall',
     'prefixes': {'SciDat': {'prefix_prefix': 'SciDat',
                             'prefix_reference': 'http://w3c.org/SciDat/'},
                  'linkml': {'prefix_prefix': 'linkml',
                             'prefix_reference': 'https://w3id.org/linkml/'},
                  'oso': {'prefix_prefix': 'oso',
                          'prefix_reference': 'http://w3c.org/oso/'}},
     'source_file': 'sila_feature_call_model.yaml'} )


class SiLAFeatureCall(ConfiguredBaseModel):
    """
    \"The SiLA Feature Call represents one single SiLA feature call to trigger a measurement.\"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/SiLA/FeatureCall'})

    feature: str = Field(..., description="""\"Fully qualified SiLA feature.\"""", json_schema_extra = { "linkml_meta": {'alias': 'feature',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:sila_feature'} })
    quantity: str = Field(..., description="""\"The quantity of the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'quantity',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementQuantity'} })
    unit: str = Field(..., description="""\"The unit of the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'unit',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementUnit'} })
    sila_call_value_raw: str = Field(..., description="""\"The SiLA call to get a single raw value the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'sila_call_value_raw',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementCallValueRaw'} })
    sila_call_value_calibrated: Optional[str] = Field(None, description="""\"The SiLA call to get a single calibrated value the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'sila_call_value_calibrated',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementCallValueCalibrated'} })
    sila_call_value_unit: Optional[str] = Field(None, description="""\"The SiLA call to get the unit of the value the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'sila_call_value_unit',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementCallValueUnit'} })
    sila_call_observable: bool = Field(..., description="""\"Is the SiLA call observable ?\"""", json_schema_extra = { "linkml_meta": {'alias': 'sila_call_observable', 'domain_of': ['SiLAFeatureCall']} })
    sila_call_metadata: str = Field(..., description="""\"The SiLA call to retrieve the metadata of the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'sila_call_metadata',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementMetadataCall'} })
    parameters: Optional[List[CallParameters]] = Field(default_factory=list, description="""\"The parameters of the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'parameters',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementParameters'} })
    poll_interval: int = Field(..., description="""\"The interval in seconds to poll the measurement.\"""", json_schema_extra = { "linkml_meta": {'alias': 'poll_interval',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementPollInterval'} })
    server_name: Optional[str] = Field(None, description="""\"The name of the SiLA server.\"""", json_schema_extra = { "linkml_meta": {'alias': 'server_name',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/SiLAServerName'} })
    server_uuid: Optional[str] = Field(None, description="""\"The UUID of the SiLA server.\"""", json_schema_extra = { "linkml_meta": {'alias': 'server_uuid',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/SiLAServerUUID'} })
    ip_address: Optional[str] = Field(None, description="""\"The IP address of the SiLA server.\"""", json_schema_extra = { "linkml_meta": {'alias': 'ip_address',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/IPAddress'} })
    port: Optional[int] = Field(None, description="""\"The port of the SiLA server.\"""", json_schema_extra = { "linkml_meta": {'alias': 'port',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/TCPPort'} })
    certificate: Optional[str] = Field(None, description="""\"The name of the certificate to authenticate the SiLA call.\"""", json_schema_extra = { "linkml_meta": {'alias': 'certificate',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementSiLACertificate'} })
    private_key: Optional[str] = Field(None, description="""\"The name of the private key to authenticate the SiLA call.\"""", json_schema_extra = { "linkml_meta": {'alias': 'private_key',
         'domain_of': ['SiLAFeatureCall'],
         'slot_uri': 'oso:measurement/measurementSiLAPrivateKey'} })


class CallParameters(ConfiguredBaseModel):
    """
    \"The parameters of the SiLA call.\"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/SiLA/FeatureCall'})

    name: str = Field(..., description="""\"The name of the parameter.\"""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['CallParameters'],
         'slot_uri': 'oso:measurement/measurementParameterName'} })
    value: str = Field(..., description="""\"The value of the parameter.\"""", json_schema_extra = { "linkml_meta": {'alias': 'value',
         'domain_of': ['CallParameters'],
         'slot_uri': 'oso:measurement/measurementParameterValue'} })


class SiLAFeatureCalls(ConfiguredBaseModel):
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/SiLA/FeatureCall', 'tree_root': True})

    SiLAFeatureCalls: Optional[Dict[str, SiLAFeatureCall]] = Field(default_factory=dict, json_schema_extra = { "linkml_meta": {'alias': 'SiLAFeatureCalls', 'domain_of': ['SiLAFeatureCalls']} })


# Model rebuild
# see https://pydantic-docs.helpmanual.io/usage/models/#rebuilding-a-model
SiLAFeatureCall.model_rebuild()
CallParameters.model_rebuild()
SiLAFeatureCalls.model_rebuild()

