#_____________________________________________________________________
#  :PROJECT: SciDat

#  SiLA Feature Call LinkML Metadata Model 

#  to convert the model to a pydantic model, use:
#     gen-pydantic --pydantic-version 2 sila_feature_call_model.yaml > ../labdata_aggregator/sila_feature_call_model.py
#     gen-doc sila_feature_call_model.yaml -d ../docs//sila_feature_call_config

# :authors: mark doerr <mark.doerr@uni-greifswald.de>
#________________________________________________________________________

id: https://w3id.org/SiLA/FeatureCall
name: SiLAFeatureCall
description: SiLA feature call.
license: https://creativecommons.org/publicdomain/zero/1.0/
version: 0.0.1

prefixes:
  linkml: https://w3id.org/linkml/
  oso: http://w3id.org/oso/
  SciDat: http://w3c.org/SciDat/

imports:
  - linkml:types

default_range: string

classes:
  SiLAFeatureCall:
    description: >-
      "The SiLA Feature Call represents one single SiLA feature call to trigger a measurement."
    slots:
      - feature
      - quantity
      - unit
      - sila_call_value_raw
      - sila_call_value_calibrated
      - sila_call_value_unit
      - sila_call_observable
      - sila_call_metadata
      - parameters
      - poll_interval
      - server_name
      - server_uuid
      - ip_address
      - port
      - certificate
      - private_key
      
  CallParameters:
    description: >-
      "The parameters of the SiLA call."
    attributes:
      name:
        description: >-
          "The name of the parameter."
        range: string
        required: true
        slot_uri: oso:measurement/measurementParameterName
      value:
        description: >-
          "The value of the parameter."
        range: string
        required: true
        slot_uri: oso:measurement/measurementParameterValue

  SiLAFeatureCalls:
    tree_root: true
    attributes:
      SiLAFeatureCalls:
        multivalued: true
        inlined: true
        inlined_as_list: false
        range: SiLAFeatureCall

slots:
  feature:
    description: >-
      "Fully qualified SiLA feature."
    identifier: true
    range: string
    required: true
    slot_uri: oso:sila_feature
  quantity:
    description: >-
      "The quantity of the measurement."
    range: string
    required: true
    slot_uri: oso:measurement/measurementQuantity
  unit:
    description: >-
      "The unit of the measurement."
    range: string
    required: true
    slot_uri: oso:measurement/measurementUnit
  sila_call_value_raw:
    description: >-
      "The SiLA call to get a single raw value the measurement."
    range: string
    required: true
    slot_uri: oso:measurement/measurementCallValueRaw
  sila_call_value_calibrated:
    description: >-
      "The SiLA call to get a single calibrated value the measurement."
    range: string
    required: false
    slot_uri: oso:measurement/measurementCallValueCalibrated
  sila_call_value_unit:
    description: >-
      "The SiLA call to get the unit of the value the measurement."
    range: string
    required: false
    slot_uri: oso:measurement/measurementCallValueUnit
  sila_call_observable:
    description: >-
      "Is the SiLA call observable ?"
    range: boolean
    required: true
  sila_call_metadata:
    description: >-
      "The SiLA call to retrieve the metadata of the measurement."
    range: string
    required: true
    slot_uri: oso:measurement/measurementMetadataCall
  poll_interval:
    description: >-
      "The interval in seconds to poll the measurement."
    range: integer
    required: true
    slot_uri: oso:measurement/measurementPollInterval
  server_name:
    description: >-
      "The name of the SiLA server."
    range: string
    required: false
    slot_uri: oso:measurement/SiLAServerName
  server_uuid:
    description: >-
      "The UUID of the SiLA server."
    range: string
    required: false
    slot_uri: oso:measurement/SiLAServerUUID
  ip_address:
    description: >-
      "The IP address of the SiLA server."
    range: string
    required: false
    slot_uri: oso:measurement/IPAddress
  port:
    description: >-
      "The port of the SiLA server."
    range: integer
    required: false
    slot_uri: oso:measurement/TCPPort
  certificate:
    description: >-
      "The name of the certificate to authenticate the SiLA call."
    range: string
    required: false
    slot_uri: oso:measurement/measurementSiLACertificate
  private_key:
    description: >-
      "The name of the private key to authenticate the SiLA call."
    range: string
    required: false
    slot_uri: oso:measurement/measurementSiLAPrivateKey
 
  
  
  
  parameters:
    description: >-
      "The parameters of the measurement."
    range: CallParameters
    multivalued: true
    slot_uri: oso:measurement/measurementParameters


