
# pytest setup and teardown
# run tests with:
#   pytest --durations=0 -s -vv --num-instances 2 .

import asyncio
import pytest
import pytest_asyncio

def pytest_addoption(parser):
    parser.addoption("--host", action="store", default="localhost")
    parser.addoption("--port", action="store", default=50066)
    parser.addoption("--token", action="store", default="Cn37d7K8GV31UYwQfLbAurcNaangUs29GDTwLl-vhUfdcsto5cLgBRyTN0-LEZf4-CU4DItjDfuC5vLEP5L9aQ==")
    parser.addoption("--bucket", action="store", default="LARAbucket")
    parser.addoption("--org", action="store", default="org.lara")
    parser.addoption("--num-instances", action="store", default=1, help="Number of instances to create test")


@pytest.fixture(scope="session")
def num_instances(pytestconfig):
    return int(pytestconfig.getoption("num_instances"))


@pytest.fixture(scope='session')
def event_loop():
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


# @pytest.fixture(scope="module")
# def method_ids(pytestconfig):
    

#     yield method_ids