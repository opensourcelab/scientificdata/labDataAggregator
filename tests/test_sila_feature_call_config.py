
"""Tests for `labdata_aggregator` package."""
# pylint: disable=redefined-outer-name
import pytest
from labdata_aggregator.sila_feature_call_model import SiLAFeatureCalls



@pytest.fixture(scope="module")
def sila_feature_call():
    """Create a SiLAFeatureCallModel object"""
    yield dict(
        feature = "de.unigreifswald/instruments/TemperatureController/v1",
        quantity = "temperature",
        unit = "K",
        sila_call_value_raw = "CurrentTemperature.get",
        sila_call_observable = False,
        sila_call_metadata = "Metadata.get",
        poll_interval = 1,
        parameters = [{"name": "temperature", "value": "300"}],
    )

def test_sila_feature_call_config(sila_feature_call):
    """Test SilaFeatureCallModel config"""

    sfcs = SiLAFeatureCalls(SiLAFeatureCalls={"de.unigreifswald/instruments/TemperatureController/v1": sila_feature_call})

    assert sfcs.SiLAFeatureCalls["de.unigreifswald/instruments/TemperatureController/v1"].feature == "de.unigreifswald/instruments/TemperatureController/v1"