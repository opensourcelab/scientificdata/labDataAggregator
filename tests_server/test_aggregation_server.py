
import pytest
import pytest_asyncio


# @pytest_asyncio.fixture(scope="module")
# async def data_minimal(data_client, num_instances, namespace_ids, datatype_ids):

class TestAggregationServer:
    #@pytest.mark.skip(reason="Not implemented")
    def test_get_FeatureCallConfiguration(self, labdata_aggregator_sila_client):
        feature_call_config = labdata_aggregator_sila_client.MeasurementController.FeatureCallConfiguration.get()
        assert feature_call_config == """SiLAFeatureCalls:
  de.unigreifswald/instruments/LightIntensityController/v1:
    certificate: null
    feature: de.unigreifswald/instruments/LightIntensityController/v1
    parameters:
    - name: light_intensity
      value: '1000.0'
    poll_interval: 5
    private_key: null
    quantity: light intensity
    sila_call_metadata: LightIntensityController.Metadata.get
    sila_call_observable: false
    sila_call_value_calibrated: null
    sila_call_value_raw: LightIntensityController.CurrentLightIntensity.get
    sila_call_value_unit: null
    unit: lux
  de.unigreifswald/instruments/TemperatureController/v1:
    certificate: null
    feature: de.unigreifswald/instruments/TemperatureController/v1
    parameters:
    - name: temperature
      value: '25.0'
    poll_interval: 5
    private_key: null
    quantity: temperature
    sila_call_metadata: TemperatureController.Metadata.get
    sila_call_observable: false
    sila_call_value_calibrated: null
    sila_call_value_raw: TemperatureController.CurrentTemperature.get
    sila_call_value_unit: null
    unit: K
"""

    # def test_get_DataOutputFormats(self, labdata_aggregator_sila_client):
    #     DataOutputFormats = labdata_aggregator_sila_client.AggregationServer.DataOutputFormats.get()
    #     assert DataOutputFormats == ['json', 'csv', 'xml']

    # def test_get_AutoMeasurementTypes(self, labdata_aggregator_sila_client):
    #     AutoMeasurementTypes = labdata_aggregator_sila_client.AggregationServer.AutoMeasurementTypes.get()
    #     assert AutoMeasurementTypes == ['temperature', 'humidity', 'pressure']

    # def test_SetStandardAggregationDataFormat(self, labdata_aggregator_sila_client):
    #     DataFormat = 'json'
    #     SetStandardAggregationDataFormat = labdata_aggregator_sila_client.AggregationServer.SetStandardAggregationDataFormat(DataFormat=DataFormat)
    #     assert SetStandardAggregationDataFormat == 'Data format set to json'

    # def test_SetAutoAggregation(self, labdata_aggregator_sila_client):
    #     AutoAggregate = True
    #     SetAutoAggregation = labdata_aggregator_sila_client.AggregationServer.SetAutoAggregation(AutoAggregate=AutoAggregate)
    #     assert SetAutoAggregation == 'Auto aggregation set to True'

    # def test_AggregateData(self, labdata_aggregator_sila_client):
    #     Bucket = 'LARAbucket'
    #     Measurement = 'de.unigreifswald/lara/arduino_demo'
    #     Factor = 'temperature'
    #     Device = 'arduino'
    #     StartTime = '2021-07-01T00:00:00Z'
    #     EndTime = '2021-07-01T00:10:00Z'
    #     DataFormat = 'json'
    #     AggregationInterval = 1.0
    #     metadata = {'key': 'value'}
    #     instance = 'ObservableCommandInstance'
    #     AggregateData = labdata_aggregator_sila_client.AggregationServer.AggregateData(Bucket=Bucket, Measurement=Measurement, Factor=Factor, Device=Device, StartTime=StartTime, EndTime=EndTime, DataFormat=DataFormat, AggregationInterval=AggregationInterval, metadata=metadata, instance=instance)
    #     assert AggregateData == 'Data aggregated successfully'


